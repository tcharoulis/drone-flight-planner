import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MapComponent} from './map/map.component';
import {FlightPlanListComponent} from './flight-plan-list/flight-plan-list.component';

const appRoutes: Routes = [
  { path: 'flight-plan/new', component: MapComponent },
  { path: 'flight-plan/:id', component: MapComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
