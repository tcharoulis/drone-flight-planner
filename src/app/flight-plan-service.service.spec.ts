import { TestBed } from '@angular/core/testing';

import { FlightPlanService } from './flight-plan.service';

describe('FlightPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlightPlanService = TestBed.get(FlightPlanService);
    expect(service).toBeTruthy();
  });
});
