import { Injectable } from '@angular/core';
import {FlightPlan, FlightPlanPoint} from './FlightPlan';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FlightPlanService {

  static nextPlanId = 100;

  private FLIGHT_PLANS: FlightPlan[] = [];

  private flightPlans$: BehaviorSubject<FlightPlan[]> = new BehaviorSubject<FlightPlan[]>(this.FLIGHT_PLANS);

  constructor() { }


  getFlightPlans() {
    return this.flightPlans$;
  }

  createFlightPlan(name: string, flightPlanPoints: FlightPlanPoint[]): Observable<FlightPlan> {
    const id = FlightPlanService.nextPlanId++;
    this.FLIGHT_PLANS.push({id: id, name: `${name} (${id})`, points: flightPlanPoints});
    return this.getFlightPlan(id);
  }

  updateFlightPlan(id: number, flightPlanPoints: FlightPlanPoint[]): Observable<FlightPlan> {
    this.getFlightPlan(id).subscribe(flightPlan => flightPlan.points = flightPlanPoints);

    return this.getFlightPlan(id);
  }

  getFlightPlan(id: number | string) {
    return this.getFlightPlans().pipe(
      map(flightPlans => flightPlans.find(flightPlan => flightPlan.id === +id))
    );
  }

  updateFlightPlanName(id: number | string, name: string): Observable<FlightPlan> {
    const flightPlan = this.getFlightPlan(id);
    flightPlan.subscribe((fp) => fp.name = name);
    return this.getFlightPlan(id);
  }

  deleteFlightPlan(id: number | string): void {
    this.getFlightPlan(id).subscribe(plan => {
      const idx = this.FLIGHT_PLANS.indexOf(plan);
      this.FLIGHT_PLANS.splice(idx, 1);
    });
  }
}
