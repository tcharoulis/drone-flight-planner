import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { FlightPlanListComponent } from './flight-plan-list/flight-plan-list.component';
import { FlightPlansComponent } from './flight-plans/flight-plans.component';
import { AppRoutingModule } from './app-routing.module';
import { FlightPlanEditComponent } from './flight-plan-edit/flight-plan-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    FlightPlanListComponent,
    FlightPlansComponent,
    FlightPlanEditComponent
  ],
  imports: [
    BrowserModule,
    LeafletModule.forRoot(),
    LeafletDrawModule.forRoot(),
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
