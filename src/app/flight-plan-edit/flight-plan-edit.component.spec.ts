import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightPlanEditComponent } from './flight-plan-edit.component';
import {FormsModule} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FlightPlanService} from '../flight-plan.service';
import {ActivatedRouteStub} from '../../testing/ActivatedRouteStub';

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
const flightPlanService = jasmine.createSpyObj('FlightPlanService',
  ['getFlightPlan', 'updateFlightPlanName']);
const activatedRoute = new ActivatedRouteStub();

describe('FlightPlanEditComponent', () => {
  let component: FlightPlanEditComponent;
  let fixture: ComponentFixture<FlightPlanEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightPlanEditComponent ],
      imports: [FormsModule],
      providers: [
        { provide: FlightPlanService, useValue: flightPlanService },
        { provide: Router,      useValue: routerSpy },
        { provide: ActivatedRoute, useValue: activatedRoute }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightPlanEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
