import {Component, Input, OnInit} from '@angular/core';
import {FlightPlan} from '../FlightPlan';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {FlightPlanService} from '../flight-plan.service';
import {switchMap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-flight-plan-edit',
  templateUrl: './flight-plan-edit.component.html',
  styleUrls: ['./flight-plan-edit.component.css']
})
export class FlightPlanEditComponent implements OnInit {

  private flightPlanId: number;
  flightPlanName: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private service: FlightPlanService) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.service.getFlightPlan(params.get('id')))
    ).subscribe((flightPlan) => this.setFlightPlan(flightPlan));
  }

  private setFlightPlan(flightPlan: FlightPlan) {
    if (flightPlan) {
      this.flightPlanId = flightPlan.id;
      this.flightPlanName = flightPlan.name;
    } else {
      this.flightPlanName = undefined;
      this.flightPlanId = undefined;
    }
  }

  updateFlightPlanName() {
    if (this.flightPlanId && this.flightPlanName) {
      this.service.updateFlightPlanName(this.flightPlanId, this.flightPlanName)
        .subscribe((fp) => this.router.navigate(['./', {id: fp.id}], {relativeTo: this.route}));
    }
  }

}
