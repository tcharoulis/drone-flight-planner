import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {Component} from '@angular/core';

@Component({selector: 'app-map', template: ''})
class MapStubComponent {}

@Component({selector: 'app-flight-plan-list', template: ''})
class FlightPlanListStubComponent {}

@Component({selector: 'app-flight-plans', template: ''})
class FlightPlansStubComponent {}

@Component({selector: 'app-flight-plan-edit', template: ''})
class FlightPlanEditStubComponent {}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MapStubComponent,
        FlightPlanListStubComponent,
        FlightPlansStubComponent,
        FlightPlansStubComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'drone-flight-planner'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('drone-flight-planner');
  }));
});
