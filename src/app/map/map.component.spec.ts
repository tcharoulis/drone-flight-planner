import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {latLng, tileLayer, polyline, LayerGroup, Polyline, Map, LatLng, FeatureGroup, SVG} from 'leaflet';
import {MapComponent} from './map.component';
import {element} from 'protractor';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import {LeafletDrawModule} from '@asymmetrik/ngx-leaflet-draw';
import {FlightPlanService} from '../flight-plan.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivatedRouteStub} from '../../testing/ActivatedRouteStub';
import {FlightPlanEditComponent} from '../flight-plan-edit/flight-plan-edit.component';
import {FormsModule} from '@angular/forms';
import {FlightPlan} from '../FlightPlan';
import {of} from 'rxjs';
import pointsToPath = SVG.pointsToPath;

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
const flightPlanService = jasmine.createSpyObj('FlightPlanService',
  ['getFlightPlan', 'updateFlightPlanName', 'createFlightPlan']);
const activatedRoute = new ActivatedRouteStub();


describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent, FlightPlanEditComponent ],
      imports: [
        FormsModule,
        LeafletModule.forRoot(),
        LeafletDrawModule.forRoot()
      ],
      providers: [
        { provide: FlightPlanService, useValue: flightPlanService },
        { provide: Router,      useValue: routerSpy },
        { provide: ActivatedRoute, useValue: activatedRoute }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call flightPlanService when a new flight plan is requested', () => {
    const stubValue: FlightPlan = {id: 1, name: '', points: []};
    flightPlanService.createFlightPlan.and.returnValue(of(stubValue));

    const p: Polyline = polyline([latLng(1, 2), latLng(3, 4)]);
    component.newFlightPlanRequested({
      layer: p,
      layerType: 'polyline',
    });

    expect(flightPlanService.createFlightPlan.calls.count())
      .toBe(1, 'spy method was called once');
    expect(routerSpy.navigate.calls.count()).toBe(1, 'navigate not called after save');
  });
});
