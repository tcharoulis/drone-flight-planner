import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {latLng, tileLayer, polyline, LayerGroup, Polyline, Map, LatLng, FeatureGroup} from 'leaflet';
import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {FlightPlan, FlightPlanPoint} from '../FlightPlan';
import {FlightPlanService} from '../flight-plan.service';
import {FeatureCollection, LineString} from 'geojson';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  options;
  drawOptions;
  private featureGroup: FeatureGroup;
  private flightPlan$: Observable<FlightPlan>;
  private flightPlan: FlightPlan;
  private map: Map;
  private currentFlightPlan: Polyline;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private service: FlightPlanService) {}

  ngOnInit() {
    this.flightPlan$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.service.getFlightPlan(params.get('id'));
      })
    );

    this.flightPlan$.subscribe(flightPlan => this.flightPlan = flightPlan);

    this.initMap();
  }

  private initMap() {
    this.featureGroup = new FeatureGroup();
    this.options = {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '&copy; OpenStreetMap contributors'
        })
      ],
      zoom: 7,
      center: latLng([46.879966, -121.726909])
    };

    this.drawOptions = {
      position: 'topright',

      draw: {
        marker: false,
        polygon: false,
        circle: false,
        circlemarker: false,
        rectangle: false
      }
      ,

      edit: {
        featureGroup: this.featureGroup,
        edit: {
          selectedPathOptions: {
            color: '#ff0000'
          }
        },
        remove: false
      }
    };
  }

  onDrawReady(e): void {
    this.map = e._map;

    this.flightPlan$.subscribe((flightPlan) => this.loadFlightPlan(flightPlan));

    this.map.on('draw:edited', (event: Edited) => this.flightPlanEdited(event));
    this.map.on('draw:created', (event: Created) => this.newFlightPlanRequested(event));
  }

  newFlightPlanRequested(event: LineCreated) {
    if (event.layerType === 'polyline') {

      const flightPlanPoints: FlightPlanPoint[] = this.fromLatLngs(event.layer.getLatLngs() as LatLng[]);

      this.service.createFlightPlan('flight_plan', flightPlanPoints)
        .subscribe((flightPlan) => this.goToFlightPlan(flightPlan.id));
    }
  }

  flightPlanEdited(event: Edited) {
    const flightPlanPoints: FlightPlanPoint[] = (event.layers.toGeoJSON() as FeatureCollection).features
      .map(feature => (feature.geometry as LineString).coordinates.map((v: number[]) => {
        return {
          longitude: v[0],
          latitude: v[1]
        }; }
      ))[0];

    this.service.updateFlightPlan(this.flightPlan.id, flightPlanPoints)
      .subscribe((flightPlan) => this.goToFlightPlan(flightPlan.id));
  }

  private fromLatLngs(latLngs: LatLng[]): FlightPlanPoint[] {
    return latLngs.map((latlng) => {
      return {
        latitude: latlng.lat,
        longitude: latlng.lng
      };
    });
  }

  private goToFlightPlan(flightPlanId: number) {
    return this.router.navigate([`flight-plan/${flightPlanId}`]);

  }

  loadFlightPlan(flightPlan) {

    if (this.currentFlightPlan) {
      this.currentFlightPlan.removeFrom(this.map);
      this.featureGroup.clearLayers();
    }

    if (flightPlan) {
      const flightPoints = flightPlan.points.map((fp) => latLng(fp.latitude, fp.longitude));

      this.currentFlightPlan = polyline(flightPoints);
      this.currentFlightPlan.addTo(this.map);
      this.featureGroup.addLayer(this.currentFlightPlan);

      this.map.fitBounds(this.currentFlightPlan.getBounds());
    }
  }
}

export interface LineCreated {
  layer:  Polyline;
  layerType: string;
}

interface Created extends Event {
  /**
   * Layer that was just created.
   */
  layer:  Polyline;

  /**
   * The type of layer this is. One of: polyline, polygon, rectangle, circle, marker.
   */
  layerType: string;

}

interface Edited extends Event {

  /**
   * List of all layers just edited on the map.
   */
  layers: LayerGroup;
}




