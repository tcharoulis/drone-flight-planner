export interface FlightPlan {
  id: number;
  name: string;
  points: FlightPlanPoint[];
}

export interface FlightPlanPoint {
  latitude: number;
  longitude: number;
}
