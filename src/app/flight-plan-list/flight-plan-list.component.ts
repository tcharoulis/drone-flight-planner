import { Component, OnInit } from '@angular/core';
import {FlightPlan} from '../FlightPlan';
import {FlightPlanService} from '../flight-plan.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-flight-plan-list',
  templateUrl: './flight-plan-list.component.html',
  styleUrls: ['./flight-plan-list.component.css']
})
export class FlightPlanListComponent implements OnInit {

  flightPlans$: Observable<FlightPlan[]>;
  selectedId: number;

  constructor(
    private service: FlightPlanService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.flightPlans$ = this.route.paramMap.pipe(
      switchMap(() => {
        return this.service.getFlightPlans();
      })
    );
  }

  select(flightPlan: FlightPlan): void {
    this.selectedId = flightPlan.id;
  }

  delete(flightPlan: FlightPlan): void {
    console.log('Deleting flight plan with id ' + flightPlan.id);
    this.service.deleteFlightPlan(flightPlan.id);
    this.router.navigate(['flight-plan/new'], {relativeTo: this.route});
  }

  requestNewFlightPlan(): void {
    this.selectedId = undefined;
    this.router.navigate(['flight-plan/new'], {relativeTo: this.route});
  }

}
