import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import { FlightPlanListComponent } from './flight-plan-list.component';
import {ActivatedRouteStub} from '../../testing/ActivatedRouteStub';
import {FlightPlanService} from '../flight-plan.service';
import {ActivatedRoute, Router} from '@angular/router';

const routerSpy = jasmine.createSpyObj('Router', ['navigate']);
const flightPlanService = jasmine.createSpyObj('FlightPlanService',
  ['getFlightPlan', 'updateFlightPlanName']);
const activatedRoute = new ActivatedRouteStub();

describe('FlightPlanListComponent', () => {
  let component: FlightPlanListComponent;
  let fixture: ComponentFixture<FlightPlanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightPlanListComponent ],
      providers: [
        { provide: FlightPlanService, useValue: flightPlanService },
        { provide: Router,      useValue: routerSpy },
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightPlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
